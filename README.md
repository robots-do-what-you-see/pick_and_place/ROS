# Pick-and-Place ROS

## Clone and Init submodule
```
git clone https://gitlab.com/robots-do-what-you-see/pick_and_place/ROS.git
git submodule update --init
```
## Build with Docker
```
docker build -t unity-robotics:pick-and-place -f docker/Dockerfile .
```
or
```
./build_image.sh
```

## Run Planner Service
```
docker run -it --rm -p 10000:10000 unity-robotics:pick-and-place roslaunch niryo_moveit part_3.launch
```
or
```
./launch_planner.sh
```

## Setup Local Workspace
```sh
# install ROS Noetic, build-essential and dependencies
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install ros-noetic-ros-base

sudo apt install -y build-essential python-is-python3 \
    ros-noetic-robot-state-publisher ros-noetic-moveit ros-noetic-rosbridge-suite \
    ros-noetic-joy ros-noetic-ros-control ros-noetic-ros-controllers \
    ros-noetic-tf2-web-republisher \
    python3-rospkg python3-jsonpickle

# clone repo && init submodule
git clone https://gitlab.com/robots-do-what-you-see/pick_and_place/ROS.git catkin_ws
git submodule update --init

# setup ros
source /opt/ros/noetic/setup.bash
# build
catkin_make
# launch
source ./devel/setup.bash
roslaunch niryo_moveit part_3.launch
```
or launch with script
```
./roslaunch_planner.sh
```